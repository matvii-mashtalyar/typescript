class Card implements Movie {
  id: number;
  poster_path: string | null;
  overview: string;
  release_date: string;
  title: string;
  backdrop_path: string | null;
  constructor(id: number, poster_path: string | null, overview: string, release_date: string, title: string, backdrop_path: string | null) {
    this.id = id;
    this.poster_path = poster_path;
    this.overview = overview;
    this.release_date = release_date;
    this.title = title;
    this.backdrop_path = backdrop_path;
  }
  render(): string {
    return (`<div class="col-lg-3 col-md-4 col-12 p-2" data-id="${this.id}">
    <div class="card shadow-sm">
      <img
        src="${this.poster_path ? 'https://image.tmdb.org/t/p/original/' + this.poster_path : 'https://via.placeholder.com/300'}"
      />
      <svg
        xmlns="http://www.w3.org/2000/svg"
        stroke="red"
        fill="#ff000078"
        width="50"
        height="50"
        class="bi bi-heart-fill position-absolute p-2"
        viewBox="0 -2 18 22"
      >
        <path
          fill-rule="evenodd"
          d="M8 1.314C12.438-3.248 23.534 4.735 8 15-7.534 4.736 3.562-3.248 8 1.314z"
        />
      </svg>
      <div class="card-body">
        <p class="card-text truncate">
          ${this.overview}
        </p>
        <div
          class="
            d-flex
            justify-content-between
            align-items-center
          "
        >
          <small class="text-muted">${this.release_date}</small>
        </div>
      </div>
    </div>
  </div>`)
  }
  renderRandomCard(): string {
    return `<style>#random-movie { background: center / cover no-repeat url(${this.backdrop_path ? 'https://image.tmdb.org/t/p/original/' + this.backdrop_path : 'https://via.placeholder.com/300'}); }</style>
    <div class="row py-lg-5">
    <div class="col-lg-6 col-md-8 mx-auto" style="background-color: #2525254f">
      <h1 id="random-movie-name" class="fw-light text-light">${this.title}</h1>
      <p id="random-movie-description" class="lead text-white">
        ${this.overview}
      </p>
    </div>
  </div>`
  }
}

export default Card;