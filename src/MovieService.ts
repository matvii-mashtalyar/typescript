import Card from './Card';

interface JSONMovie {
  poster_path: string | null,
  adult: boolean,
  overview: string,
  release_date: string,
  genre_ids: number[],
  id: number,
  original_title: string,
  original_language: string,
  title: string,
  backdrop_path: string | null,
  popularity: number,
  vote_count: number,
  video: boolean,
  vote_average: number
};

class MovieService {
  static page: number = 1;
  static url: string;

  private formatMovie(movie: JSONMovie): Card {
    return new Card(movie.id, movie.poster_path, movie.overview, movie.release_date, movie.title, movie.backdrop_path);
  }

  private async request(url: string): Promise<Card[]> {
    return await fetch(url + `&page=${MovieService.page}`)
      .then(res => res.json())
      .then(res => res.results.map((movie: JSONMovie) => this.formatMovie(movie)))
  }

  private setUrl(url: string): void {
    MovieService.url = url
  }

  firstPage(): void {
    MovieService.page = 1;
  }

  nextPage(): Promise<Card[]> {
    MovieService.page++;
    return this.request(MovieService.url)
  }

  searchByTitle(title: string): Promise<Card[]> {
    this.setUrl(`https://api.themoviedb.org/3/search/movie?api_key=bd720987fd61269199c407c82484ed72&query=${title}`);

    return this.request(MovieService.url)
  }
  getPopular(): Promise<Card[]> {
    this.setUrl(`https://api.themoviedb.org/3/movie/popular?api_key=bd720987fd61269199c407c82484ed72`)

    return this.request(MovieService.url)
  }
  getTopRated(): Promise<Card[]> {
    this.setUrl(`https://api.themoviedb.org/3/movie/top_rated?api_key=bd720987fd61269199c407c82484ed72`)

    return this.request(MovieService.url)
  }
  getUpcoming(): Promise<Card[]> {
    this.setUrl(`https://api.themoviedb.org/3/movie/upcoming?api_key=bd720987fd61269199c407c82484ed72`)

    return this.request(MovieService.url)
  }
}

export default MovieService;