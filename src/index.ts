import Card from "./Card";
import MovieService from "./MovieService";

export async function render(): Promise<void> {
	// TODO render your app here
	const apiClient = new MovieService();

	const filmContainer = document.getElementById('film-container');
	const randomMovieContainer = document.getElementById('random-movie');

	function renderCards(cards: Card[]): void {
		cards.map(card => {
			filmContainer?.insertAdjacentHTML('beforeend', card.render())
		})
	}

	function renderMovies(): void {
		const inputs: NodeListOf<HTMLInputElement> = document.querySelectorAll('input[name="btnradio"]');
		let checkedInput: HTMLInputElement;
		let getMovies: Promise<Card[]>;

		inputs.forEach(input => {
			if (input.checked) {
				checkedInput = input;

				if (checkedInput === <HTMLInputElement>document.getElementById('popular')) {
					getMovies = apiClient.getPopular()
				}
				else if (checkedInput === <HTMLInputElement>document.getElementById('upcoming')) {
					getMovies = apiClient.getUpcoming()
				}
				else if (checkedInput === <HTMLInputElement>document.getElementById('top_rated')) {
					getMovies = apiClient.getTopRated()
				}

				getMovies.then(movies => {
					renderCards(movies);
					renderRandomMovie(movies);

				});
			}
		})
	}

	function clear(): void {
		let child = filmContainer?.lastElementChild;
		while (child) {
			filmContainer?.removeChild(child);
			child = filmContainer?.lastElementChild;
		}

		child = randomMovieContainer?.lastElementChild;
		while (child) {
			randomMovieContainer?.removeChild(child);
			child = randomMovieContainer?.lastElementChild;
		}
	}

	function getRandomInt(max: number): number {
		return Math.floor(Math.random() * max);
	}

	function getRandomMovie(movies: Card[]): Card {
		const n: number = getRandomInt(movies.length);
		return movies[n];
	}

	function renderRandomMovie(movies: Card[]): void {
		randomMovieContainer?.insertAdjacentHTML('beforeend', getRandomMovie(movies).renderRandomCard());
	}

	document.getElementById('submit')?.addEventListener('click', () => {

		const searchValue = (<HTMLInputElement>document.getElementById('search')).value;

		if (!searchValue) {
			return;
		}

		clear();
		apiClient.searchByTitle(searchValue).then(movies => {
			renderCards(movies);
			renderRandomMovie(movies);
		})

	})

	window.onload = renderMovies;

	const radioWrapper = document.getElementById('button-wrapper');

	radioWrapper?.addEventListener('change', () => {

		clear();
		apiClient.firstPage();
		renderMovies();

	})

	const loadMoreBtn = document.getElementById('load-more');
	loadMoreBtn?.addEventListener('click', () => {

		apiClient.nextPage().then(movies => renderCards(movies));

	})
}
