interface Movie {
  id: number;
  poster_path: string | null;
  overview: string;
  release_date: string;
  title: string;
  backdrop_path: string | null;
}